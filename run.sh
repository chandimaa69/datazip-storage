#! /bin/bash

echo -e "Enter the file path : \c"
read file_name

export MYSQL_PWD="nmBQ4v5E"

# check file isExist
if [ ! -f $file_name ] 
then
    echo "Cannot find '$file_name' file."
    exit -1    
fi


# 2nd if statement
if [ ${file_name: -4}  == ".zip" ]
then
    # 3rd if statement
    if [ -r $file_name ]
    then
        tempDir="temp"

        # unzip file
        unzip $file_name -d temp

        for entry in "$tempDir"/*
        do
            if [ ${entry: -4} == ".csv" ]
            then
                # check read permission 
                if [ ! -r "$tempDir" ]
                then
                    chmod 644 $tempDir
                fi

                # create temp file path for .csv files
                tempCSVFile="$( cd "$( dirname "$0" )" && pwd )/$entry"

                # check read permission 
                if [ ! -r $tempCSVFile ]
                then
                    chmod 644 $tempCSVFile
                fi
        
                mysql -u root -h 127.0.0.1 -D data_db -e "LOAD DATA INFILE '$tempCSVFile' IGNORE INTO TABLE data COLUMNS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' "
            fi

        done # end for loop
        rm -r $tempDir # delete temp directory
        
        exit 0

    else
        echo "Can't read this file $file_name, Permission denied."
    fi # end 3rd if statement

else
    echo "This file is not a zip file."
fi # 2nd end if statement