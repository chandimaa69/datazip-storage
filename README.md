Shell Scripting hands on assignment:

Tip 1: What you type in a Unix terminal can be written in a `.sh` file and and create an
executable .sh file. Similar to `.bat` files on Windows.

Tip 2: With `sudo` as a prefix to any command, you specify that the command should be run as root
user. Root user is the administrator.

Tip 3: `sudo <username>` will switch users in the terminal.
e.g: `sudo su` will switch you to the super user which is the root (administrator).



This assignment should be done with self exploration.

1. Clone the repo and find the zip inside the repo.
https://gitlab.com/chandimaa69/datazip-storage
2. You need to create a shell executable (.sh) which will accept zip file path in the parameters,
copy the given zip in the to /tmp dir,
unzip and for each CSV files inside the zip, file should be loaded in to a pre created MySQL db
table.

Hints:

First create the MySQL table structure: data_db → data(id, name).


Practice `print`,`echo` commands in the terminal.

* Then practice file copy(cp), move(mv), remove(rm) commands in the terminal.

* Then practice file zip, unzip commands in the terminal.

* Then practice how to execute a .sh file from the terminal.

* Then practice how to execute a .sh with parameters from the terminal.

* Then practice how to use loops (for, while) in shell, bash scripting.

* Then practice how to call `mysql -u root- p` with inline password and how to issue sql queries
inside an sh file.

* Then in your workbench or mysql shell, practice how to load a CSV file, txt file directly to a sql
table (which is very fast) using `LOAD DATA INFILE` query.

* Optional: practice how to export a CSV file from a table. (Using `SELECT INTO OUTFILE`)
command.

Now you’re ready to implement the complete .sh file. Test it and commit the .sh to repo.